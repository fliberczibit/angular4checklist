// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAtyCkwK1eDzvWHa5geVs-Um7mT2jNqn1I',
    // authDomain: '<your-project-authdomain>',
    databaseURL: 'https://cla4-58a7e.firebaseio.com/',
    projectId: 'cla4-58a7e',
  }
};
