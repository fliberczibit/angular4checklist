import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import {List} from '../model/List';
import {Person} from '../model/Person';
import {ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/first';
import {FirebaseListObservable} from 'angularfire2/database';

@Component({
  selector: 'app-edit-list',
  templateUrl: './edit-list.component.html',
  styleUrls: ['./edit-list.component.css']
})
export class EditListComponent implements OnInit {
  people: Person[];
  list: List;
  public name: string;
  public surname: string;
  private people$: FirebaseListObservable<Person[]>;
  public appear: boolean;

  constructor(private dataService: DataService,
              private route: ActivatedRoute) {
  };

  ngOnInit() {
    this.appear = false;
    this.route.paramMap
      .subscribe((params: ParamMap) => {
          const listId = params.get('id');
          this.getListData(listId);
          /*Tutaj wyciągnięty został Id z routera*/
        }
      );
  }

  private getListData(listKey: string) {
    this.people$ = this.dataService.getPeopleForList$(listKey);
    this.people$.subscribe(people => this.people = people);
    this.dataService.getList$(listKey)
      .subscribe(list => this.list = list);

  }

  // const filteredLists = this.dataService.list.filter(function (item) {
  //   return item.$key === listId;
  // });
  // /*Tutja została przefiltrowana lista w poszukiwaniu elementu Id w liście równym Id routera*/
  // this.list = filteredLists[0];
  // this.checkedPeople(this.list)
  private checkedPeople(properObject) {
    if (properObject) {
      console.log('prop', properObject);
      return properObject.filter(function (item) {
        return item.checked === true;
      });
    } else {
      return [];
    }
  }

  public isOneChecked(): boolean {
    return this.checkedPeople(this.people).length === 1;
  }

  private isMorethanZero() {
    // return this.checkedPeople(this.people).length >= 1;
  }
  public removeCheckedPeople() {
      const array = this.checkedPeople(this.people);
  return array.forEach(item => this.people$.remove(item.$key));
  }

  public addPerson(listKey: string) {
    this.people$.push({name: this.name, surname: this.surname, hasCome: false})
    this.name = '';
    this.surname = '';
  }
  public editPerson() {
    this.appear = true;
    if (this.isOneChecked()) {
      const array = this.checkedPeople(this.people)[0];
      this.name = array.name;
      this.surname = array.surname;
    }

  }
  updatePerson() {
    this.appear = true;
    const array = this.checkedPeople(this.people)[0];
    this.people$.update( array.$key, { name: this.name, surname: this.surname });
    this.name = '';
    this.surname = '';
    this.appear = false;
  }
}

/*  Tutaj przyrównujemy  zwrócony obiekt, którego Id routera jest równe Id listy */


