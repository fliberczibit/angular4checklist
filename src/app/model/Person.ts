export interface Person {
  name: string;
  surname: string;
  hasCome: boolean;
  $fullName?: string;
  email: string;
  company: string;
  mobile: string;
  checked?: boolean;
  $key: string;
}
