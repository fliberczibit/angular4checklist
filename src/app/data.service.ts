import {Injectable} from '@angular/core';
import {List} from './model/List';
import {AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2/database';
import {Person} from './model/Person';

@Injectable()
export class DataService {
  constructor(private db: AngularFireDatabase) {
  }

  public getAllLists$(): FirebaseListObservable<List[]> {
    return this.db.list('/lists');
  }

  public getPeopleForList$(listKey: string): FirebaseListObservable<Person[]> {
    return this.db.list('/people/' + listKey);
  }

  public getHasComeFalseSortedPeopleForList$(listKey: string): FirebaseListObservable<Person[]> {
    return this.db.list(('/people/' + listKey), {query: {orderByChild : 'hasCome' , equalTo: false}});
  }
  public getHasComeTrueSortedPeopleForList$(listKey: string): FirebaseListObservable<Person[]> {
    return this.db.list(('/people/' + listKey) , {query: {orderByChild : 'hasCome' , equalTo: true}});
  }
  getList$(listKey: string): FirebaseObjectObservable<List>  {
    return this.db.object('/lists/' + listKey);
  }
}
