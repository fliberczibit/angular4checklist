import {Injectable} from '@angular/core';
import * as alaSQLSpace from 'alasql';
import {DataService} from './data.service';
import {List} from './model/List';
import {FirebaseListObservable} from 'angularfire2/database';

@Injectable()
export class ImportService {
  lists$: FirebaseListObservable<List[]>;
  list: List;

  constructor(private dataService: DataService) {
    this.lists$ = this.dataService.getAllLists$();
  }

  public importElement(event: Event): alaSQLSpace.Thenable<void> {
    return alasql.promise('SELECT * FROM FILE(?,{headers:true, utf8Bom: true})', [event])
      .then(data => {
        console.log('event', event);
        const data2 = data.map(function (person) {
          return {
            'name': person.name,
            'surname': person.surname,
            'hasCome': person['has Come'],
          }
        });
        const fileName = ((event.target as any).files[0].name);
        this.lists$.push({
          name: fileName.substring(0, fileName.indexOf('.'))
        });

      })
      .catch(err => {
        console.log('Error:', err);
      });
  }
}
