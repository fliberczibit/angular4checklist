import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

const credentials = {
  login: 'a',
  password: 'a',
};
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})


export class LoginFormComponent implements OnInit {
  public username: string;
  public userPassword: string;

  formSubmit() {
      if (this.userPassword === credentials.password && this.username === credentials.login) {
        this.router.navigateByUrl('/listOfLists');
      }
  }
  constructor(private router: Router) {
  }

  ngOnInit() {
  }


}

interface UserData {
  userPassword: string;
  username: string;
}
