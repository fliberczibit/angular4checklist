import {Component, OnInit, Inject} from '@angular/core';
import {DataService} from '../data.service';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';

@Component({
  selector: 'app-dialog',
  templateUrl: './single-field-editor.component.html',
  styleUrls: ['./single-field-editor.component.css']
})
export class SingleFieldEditorComponent implements OnInit {
  public nameListChanger: string;

  constructor(private dataService: DataService,
              @Inject(MD_DIALOG_DATA) public data: string,
              public dialogRef: MdDialogRef<SingleFieldEditorComponent>) {
  }

  ngOnInit() {
    this.nameListChanger = this.data;
  }

  changeNameList() {
    this.dialogRef.close(this.nameListChanger);
  }

}
