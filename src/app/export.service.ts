import {Injectable} from '@angular/core';
import {DataService} from './data.service';
import {List} from './model/List';
import {Person} from './model/Person';
import * as alaSQLSpace from 'alasql';
import {FirebaseListObservable} from 'angularfire2/database';

@Injectable()
export class ExportService {
  people$: FirebaseListObservable<Person[]>;
  constructor(public dataService: DataService) {
  }

  public testAlaSQLExcelExport(list: List): void {
    this.people$ = this.dataService.getPeopleForList$(list.$key);
    const opts = [{sheetid: list.name, header: true}];
    this.people$.first().subscribe(person => alasql(`SELECT INTO XLSX("${list.name}.xlsx",?) FROM ?`, [opts, [person]]));
  }
}
