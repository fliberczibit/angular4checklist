import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { ListOfListsComponent } from './list-of-lists/list-of-lists.component';
import { CheckListComponent } from './check-list/check-list.component';
import { EditListComponent } from './edit-list/edit-list.component';
import { MdInputModule} from '@angular/material';
import {MyOwnCustomMaterialModule} from './app.moduleMaterial';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {DataService} from './data.service';
import {AlphabetPipe} from './alphabet.pipe';
import { SearchElementPipe } from './pipe/search-element.pipe';
import {SingleFieldEditorComponent} from 'app/single-field-editor/single-field-editor.component';
import { HasComeFilterPipe } from './pipe/has-come-filter.pipe';
import { HasNotComeFilterPipe } from './pipe/has-not-come-filter.pipe';
import { SingleFieldImportComponent } from './single-field-import/single-field-import.component';
import { OrderByPipe } from './pipe/order-by.pipe';
import {ExportService} from './export.service';
import {ImportService} from './import.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { SearchPersonPipe } from './pipe/search-person.pipe';



@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    ListOfListsComponent,
    CheckListComponent,
    EditListComponent,
    AlphabetPipe,
    SearchElementPipe,
    SingleFieldEditorComponent,
    HasComeFilterPipe,
    HasNotComeFilterPipe,
    SingleFieldImportComponent,
    OrderByPipe,
    SearchPersonPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MdInputModule,
    MyOwnCustomMaterialModule,
    HttpModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot([
      {
        path: 'checkList/:id', component: CheckListComponent
    },
      {
        path: 'loginForm', component: LoginFormComponent
      },
      {
        path: 'listOfLists', component: ListOfListsComponent
      },
      {
        path: 'editList/:id', component: EditListComponent
      },
      {
        path: '**', component: ListOfListsComponent
      },

    ])
  ],
  entryComponents: [
    SingleFieldEditorComponent,
    SingleFieldImportComponent
  ],
  providers: [DataService, ExportService, ImportService],
  bootstrap: [AppComponent]
})
export class AppModule { }
