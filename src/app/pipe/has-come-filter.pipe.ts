import {Pipe, PipeTransform} from '@angular/core';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/filter';

@Pipe({
  name: 'hasComeFilter',
  pure: false
})
export class HasComeFilterPipe implements PipeTransform {
  transform(value: any): any {
    if (!value) {
      return value;
    } else {
      return value.filter(item => item.hasCome === true);
    }
  }
}
