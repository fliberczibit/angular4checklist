import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'searchPerson'
})
export class SearchPersonPipe implements PipeTransform {

  transform(value: any, filterString: string, name: any): any {
    if (!value || value.length === 0 || filterString === '' || !filterString || value.length === undefined || filterString === undefined) {
      return value;
    }
    return value.filter(person => (person.surname.toLowerCase() + ' '
    + person.name.toLowerCase() + ' ').indexOf(filterString.toLowerCase()) >= 0 || (person.name.toLowerCase() + ' '
    + person.surname.toLowerCase() + ' ').indexOf(filterString.toLowerCase()) >= 0);
  }
}
