import {Pipe, PipeTransform} from '@angular/core';
@Pipe({
  name: 'searchElement',
  pure: false
})
export class SearchElementPipe implements PipeTransform {
  transform(value: any, filterString: string, name: any): any {
    if (!value || value.length === 0 || filterString === '' || !filterString || value.length === undefined || filterString === undefined) {
      return value;
    }
    return value.filter(list => (list.name.toLowerCase()).indexOf(filterString.toLowerCase()) >= 0);
  }
}







