import {Pipe, PipeTransform} from '@angular/core';
import 'rxjs/add/operator/filter';


@Pipe({
  name: 'hasNotComeFilter',
  pure: false
})
export class HasNotComeFilterPipe implements PipeTransform {
  transform(value: any[]): any {
    if (!value) {
      return value;
    }else {

    return value.filter(item => item.hasCome === false);
  }
}}
