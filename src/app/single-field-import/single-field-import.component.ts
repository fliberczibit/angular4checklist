import {Component, OnInit, Inject} from '@angular/core';
import {DataService} from '../data.service';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {ImportService} from 'app/import.service';

@Component({
  selector: 'app-single-field-import',
  templateUrl: './single-field-import.component.html',
  styleUrls: ['./single-field-import.component.css']
})
export class SingleFieldImportComponent implements OnInit {

  constructor(private dataService: DataService,
              private importService: ImportService,
              @Inject(MD_DIALOG_DATA) public data: string,
              public dialogRef: MdDialogRef<SingleFieldImportComponent>) {
  }

  ngOnInit() {
  }

  onFileChange(event) {
    this.importService.importElement(event)
      .then(() => this.dialogRef.close());
  }
}
