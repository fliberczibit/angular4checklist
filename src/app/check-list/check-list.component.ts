import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import {List} from '../model/List';
import {Person} from '../model/Person';
import {ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import {FirebaseListObservable} from 'angularfire2/database';

@Component({
  selector: 'app-check-list',
  templateUrl: './check-list.component.html',
  styleUrls: ['./check-list.component.css']
})
export class CheckListComponent implements OnInit {
  list: List;
  people: Person[];
  person: Person;
  searchPersonModel: string;
  people$: FirebaseListObservable<Person[]>;
  peopleNotHasCome$: FirebaseListObservable<Person[]>;
  peopleHasCome$: FirebaseListObservable<Person[]>;

  constructor(private dataService: DataService,
              private route: ActivatedRoute) {
  };

  ngOnInit() {
    this.route.paramMap
      .subscribe((params: ParamMap) => {
          const listId = params.get('id');
          this.getListData(listId);
          /*Tutaj wyciągnięty został Id z routera*/
        }
      );
  }

  private getListData(listKey: string) {
    this.people$ = this.dataService.getPeopleForList$(listKey);
    this.peopleNotHasCome$ = this.dataService.getHasComeFalseSortedPeopleForList$(listKey);
    this.peopleHasCome$ = this.dataService.getHasComeTrueSortedPeopleForList$(listKey);
    this.dataService.getList$(listKey)
      .subscribe(list => this.list = list);
  }

  public changeBooleanHasCome(person: Person) {
    this.people$.update(person.$key, {'hasCome': person.hasCome});
  }
}

/*Tutja została przefiltrowana lista w poszukiwaniu elementu Id w liście równym Id routera*/
// this.list = filteredLists[0];
/*  Tutaj przyrównujemy  zwrócony obiekt, którego Id routera jest równe Id listy */

