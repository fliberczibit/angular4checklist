import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import {List} from '../model/List';
import {Person} from '../model/Person';
import {Router} from '@angular/router';
import {MdDialog, MdIconModule, MdDialogRef} from '@angular/material';
import {SingleFieldEditorComponent} from 'app/single-field-editor/single-field-editor.component';
import {SingleFieldImportComponent} from 'app/single-field-import/single-field-import.component';
import {ExportService} from '../export.service';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';


@Component({
  selector: 'app-list-of-lists',
  templateUrl: './list-of-lists.component.html',
  styleUrls: ['./list-of-lists.component.css']
})
export class ListOfListsComponent implements OnInit {
  list: List[] = [];
  person: Person[] = [];
  lists$: FirebaseListObservable<List[]>;
  people$: FirebaseListObservable<Person[]>;
  searchElementModel: string;

  constructor(private dataService: DataService,
              private router: Router,
              private exportService: ExportService,
              public dialog: MdDialog,
              ) {
    this.lists$ = this.dataService.getAllLists$();
    // this.people$ = this.dataService.getPeopleForList$();
    // this.people$ = db.list('/people/' + id);
  };

  ngOnInit() {
    // this.list = this.dataService.list;
    // this.lists$.subscribe(items => {
    //   this.lists = items;
    //
    // })
  }


  exportList(list: List) {
    this.exportService.testAlaSQLExcelExport(list);
  }

  openList = function (id) {
    this.router.navigateByUrl(`checkList/${id}`);
  };

  openDialog(list) {
    const dialogRef = this.dialog.open(SingleFieldEditorComponent, {
      data: list.name
    });
    dialogRef.afterClosed().subscribe(result => {
      list.name = result;
    });
  }

  openImportDialog() {
    const dialogRef = this.dialog.open(SingleFieldImportComponent, {});
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  removeElementOfList(oneList: List) {
    this.lists$.remove(oneList.$key);
  }
}



