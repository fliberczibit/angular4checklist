import { CLA4Page } from './app.po';

describe('cla4 App', () => {
  let page: CLA4Page;

  beforeEach(() => {
    page = new CLA4Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
